## 一. 概述

基于`ch2601`芯片的网络音乐播放器例程，可以播放一首基于http网络应用层协议的mp3音频文件。

其基于平头哥[YoC](https://yoc.docs.t-head.cn/yocbook/Chapter1-YoC%E6%A6%82%E8%BF%B0/)软件平台中的轻量级的多媒体[av](https://yoc.docs.t-head.cn/yocbook/Chapter5-%E7%BB%84%E4%BB%B6/%E5%A4%9A%E5%AA%92%E4%BD%93%E6%92%AD%E6%94%BE%E5%99%A8/)组件来开发。av组件中内置了一个播放器的实现，可以播放mp3，mp4，flac、wav、amr等多种常见音频格式。同时基于播放器和YoC微服务(uService)架构实现了音频服务，支持提示音、语音合成(TTS)、音乐播放的状态切换与维护等功能，方便上层应用开发。

## 二. 组件安装

Linux开发环境下命令行安装如下：

```shell
yoc init
yoc install ch2601_webplayer_demo
```

Windows开发环境下请通过CDK集成开发环境搜索`ch2601_webplayer_demo`下载安装。

## 三. 主要示例代码说明

```c
static player_t *g_player;

static void _player_event(player_t *player, uint8_t type, const void *data, uint32_t len)
{
    int rc;
    UNUSED(len);
    UNUSED(data);
    UNUSED(handle);
    LOGD(TAG, "=====%s, %d, type = %d", __FUNCTION__, __LINE__, type);

    switch (type) {
    case PLAYER_EVENT_ERROR:      // 播放出错事件
        rc = player_stop(player);
        break;

    case PLAYER_EVENT_START: {    // 开始播放事件
        media_info_t minfo;
        memset(&minfo, 0, sizeof(media_info_t));
        rc = player_get_media_info(player, &minfo);  // 获取媒体时长、大小等信息
        LOGD(TAG, "=====rc = %d, duration = %llums, bps = %llu, size = %u", rc, minfo.duration, minfo.bps, minfo.size);
        break;
    }

    case PLAYER_EVENT_FINISH:     // 播放结束事件
        player_stop(player);      // 停止播放
        break;

    default:
        break;
    }
}

player_t *get_player_demo()
{
    if (!g_player) {
        ply_conf_t ply_cnf;

        player_conf_init(&ply_cnf);               // 初始化播放器默认配置
        ply_cnf.vol_en         = 1;               // 使能数字音量功能
        ply_cnf.vol_index      = 160;             // 0~255
        ply_cnf.event_cb       = _player_event;   // 播放事件回调函数
        ply_cnf.period_num     = 12;              // 底层音频输出缓冲周期，用于控制音频输出缓冲大小
        ply_cnf.cache_size     = 32 * 1024;       // 网络时的播放缓冲大小

        g_player = player_new(&ply_cnf);          // 创建播放器
    }

    return g_player;
}
```

## 四. 编译及烧录

linux开发环境请参考[此](https://yoc.docs.t-head.cn/yocbook/Chapter2-%E5%BF%AB%E9%80%9F%E4%B8%8A%E6%89%8B%E6%8C%87%E5%BC%95/%E4%BD%BF%E7%94%A8Linux%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E5%BF%AB%E9%80%9F%E4%B8%8A%E6%89%8B.html)。

windows开发环境请参考[此](https://yoc.docs.t-head.cn/yocbook/Chapter2-%E5%BF%AB%E9%80%9F%E4%B8%8A%E6%89%8B%E6%8C%87%E5%BC%95/%E4%BD%BF%E7%94%A8CDK%E5%BC%80%E5%8F%91%E5%BF%AB%E9%80%9F%E4%B8%8A%E6%89%8B.html)。

## 五. 运行使用

上电运行后，通过串口输入`player help`会有如下打印：

```shell
# player help
        player play welcom/url[http://]  #播放内置mp3音频welcom/http网络音频
        player pause                     #暂停播放
        player resume                    #恢复播放
        player stop                      #停止播放
        player help                      #帮助命令
# 
```

### 播放内置音频

```cli
player play welcom
```

### 播放网络歌曲

```cli
player play http://xx.xx.xx.xx/hello.mp3
```

其中xx.xx.xx.xx是http服务器的ip地址或域名。http服务器的搭建方法请自行搜索。

## 六. 主要依赖组件

  - at
  - av
  - drv_wifi_at_w800



# 